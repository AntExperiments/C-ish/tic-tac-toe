all: tic-tac-toe

WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O2

tic-tac-toe: Makefile tic-tac-toe.c
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) tic-tac-toe.c

clean:
	rm -f tic-tac-toe

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./tic-tac-toe

