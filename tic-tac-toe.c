
///
/// Program-Name:   Tic Tac Toe
/// Filename:       tic-tac-toe.c
/// Description:    Fully functional text-based TicTacToe game
/// Author:         Yanik Ammann, contact me at info@1yanik3.com if questions arrise
/// Last Changed:   22.11.2019 by Yanik Ammann
/// Git-Repository: https://gitlab.com/ConfusedAnt/tictactoe/
/// Prerequisites:  none
///
/// To-Do/Notes:    git commit -m "Added Icon and README" tic-tac-toe.c; git push
///
/// Versions (version-number, date, author, description):
/// v0.0, 15.11.2019, Yanik Ammann, starting developement
/// v0.1, 17.11.2019, Yanik Ammann, added checking capability
/// v0.2, 21.11.2019, Yanik Ammann, code optimisations and added comments
/// v0.3, 22.11.2019, Yanik Ammann, code optimisations and compression
/// v0.4, 24.11.2019, Yanik Ammann, compressing and optimizing code further
/// v0.5, 24.11.2019, Yanik Ammann, fixed spacing and added author-note
//
/// License:        This program is free software: you can redistribute it and/or modify
///                 it under the terms of the GNU General Public License as published by
///                 the Free Software Foundation, either version 3 of the License, or
///                 (at your option) any later version.
///
///                 You should have received a copy of the GNU General Public License
///                 along with this program.  If not, see <http://www.gnu.org/licenses/>.
///

#include <stdio.h>
#include <stdlib.h>

#define dim 3

void printField(int field[][dim]);
int check(int field[dim][dim], int userID);


int main() {
  int newSelected;
	int playField[dim][dim] = { 0 };
	printf("Successfully started TicTacToe\n\n");
	printField(playField);

  /////////////////////// Game-Loop start
	int userID = 1;
	while (1) {
		// select field
		printf("\n\nPlayer %d: Enter wich field to select (1-9): ", userID);
		scanf("%d", &newSelected); // using scanf() instead of scanf_s() for compatibility
		// check if already written on that field (no overwrites)
		if (playField[(newSelected - 1) / dim][(newSelected - 1) % dim] == 0) {
			playField[(newSelected - 1) / dim][(newSelected - 1) % dim] = userID;
		} else {
			printf("\n Field is already in use!\n");
			userID = userID % 2 + 1;
		}

		// display playField
		printField(playField);

		// check if won
		if (check(playField, userID) == 1) {
			printf("\nGAME OVER!\nCongratulations: Player %d won the game\n", userID);
			break;
		} else {
			userID = userID % 2 + 1;
		}
	}
  /////////////////////// Game-Loop end

	return EXIT_SUCCESS;
}

// prints playfield
void printField(int field[dim][dim]) {
	printf("-------------\n");
	char placementVariable[3] = { ' ', 'X', 'O' };
	for (int i = 0; i < dim; i++) { //row
    printf("|");
		for (int j = 0; j < dim; j++) //column
			printf("%2c |", placementVariable[field[i][j]]);
		printf("\n-------------\n");
	}
}


// checks if math is won (checks row, column and diagonals)
int check(int field[dim][dim], int userID) { // using int instead of bool for compatibility
	/////////////////////// section Rows start
	for (int i = 0; i < dim; i++) { //row
		int foundInRow = 0;
		for (int j = 0; j < dim; j++) //column
			if (field[i][j] == userID)
				foundInRow++;
		if (foundInRow == dim)
			return 1;
	}
	/////////////////////// section Rows end

  /////////////////////// section Columns start
	for (int i = 0; i < dim; i++) { //column
		int foundInColumn = 0;
		for (int j = 0; j < dim; j++) //row
			if (field[j][i] == userID)
				foundInColumn++;
		if (foundInColumn == dim)
			return 1;
	}
  /////////////////////// section Columns end

  /////////////////////// section Diagonals start
	// from top left to bottom right
	for (int i = 0, foundInDiagonal = 0; i < dim; i++) {
		for (int j = 0; j < dim; j++)
			if (field[j][i] == userID && i == j)
				foundInDiagonal++;
		if (foundInDiagonal == dim)
			return 1;
	}
	// from top right to bottom left
	for (int i = 0, foundInDiagonal = 0; i < dim; i++) {
		for (int j = 0; j < dim; j++)
			if (field[j][i] == userID && i + j == dim -1)
				foundInDiagonal++;
		if (foundInDiagonal == dim)
			return 1;
	}
  /////////////////////// section Diagonals end

	return 0;
}

